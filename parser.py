# -*- coding: utf-8 -*-
from __future__ import with_statement
import codecs
import csv
import json
import collections

lang_mapping = {'Укр.': 1, 'Рус.': 2}
subject_mapping = {'Біологія': 1, 'Зарубіжна література': 2, 'Алгебра': 3, 'Економіка': 4,
                   'Екологія': 5, 'Історія України': 6, 'Литература': 7, 'Фізична культура': 8,
                   'Захист Вітчизни': 9, 'Російська мова': 10, 'Хімія': 11, 'Англійська мова': 12,
                   'Математика': 13, 'Інформатика': 14, 'Основи правознавства': 15, 'Фізика': 16,
                   'Літературне читання': 17, 'Астрономія': 18, 'Словник': 19,
                   'Українська література': 20, 'Культура': 21, 'Всесвітня історія': 22,
                   'Геометрія': 23, 'Література': 24, 'Технології': 25, 'Природознавство': 26,
                   "Основи здоров'я": 27, 'Географія': 28, 'Основи здоров’я': 29,
                   'Трудове навчання': 30, 'Філософія': 31, 'Німецька мова': 32,
                   'Українська мова': 33}

csvfile = open('/home/vitalii/projects/ruslan_books/result - result (1).csv', 'r')
with codecs.open("f.json", "w", encoding="utf-16") as jsonfile:
    jsonfile.write('[')

    fieldnames = ('name', 'author', 'description', 'subject', 'language', 'category', 'class',
                  'img_url', 'book_url', 'addition_url')

    reader = csv.DictReader(csvfile, fieldnames)
    for idx, row in enumerate(reader):
        di = collections.OrderedDict()

        di['id'] = idx
        di['name'] = row['name']
        di['author'] = row['author']
        di['description'] = row['description']
        di['language'] = lang_mapping.get(row['language'], '')
        di['category'] = int(row['category']) if row['category'].isdigit() else ''
        di['subject'] = subject_mapping.get(row['subject'], '')
        di['class'] = int(row['class']) if row['class'].isdigit() else ''
        di['img_url'] = './covers/' + row.get('img_url', '')
        di['book_url'] = './books/' + row.get('book_url', '')
        addition_url = row.get('addition_url', '')
        di['addition_url'] = './addition/' + addition_url if addition_url else ''
        jsonfile.write(json.dumps(di))
        jsonfile.write(',')
        jsonfile.write('\n')
    jsonfile.write(']')
