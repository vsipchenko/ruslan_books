String.prototype.format = function() {
  var a = this;
  for (k in arguments) {
    a = a.replace("{" + k + "}", arguments[k])
  }
  return a
};

function satisfiesSearch(obj){
    var name_matches = obj['name'].toLowerCase().includes(this.search_string.toLowerCase());
    var author_matches = obj['author'].toLowerCase().includes(this.search_string.toLowerCase());
    return name_matches || author_matches
}

function satisfiesClass(obj){
    return obj['class'] === this.class_num
}

function satisfiesLanguage(obj) {
    return obj['language'] === this.language
}

function satisfiesSubject(obj) {
    return obj['subject'] === this.subject
}

function categoryFilter(obj){
    return this.categories.includes(obj['category'])
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


var categories = parseInt(getParameterByName('category')) ;

if (categories){
    var className = "btn-category-" + categories;
    document.getElementById(className).classList.add("active-category");
}

function loadBooks() {
    var books_copy = books.filter(categoryFilter, {'categories': categories ? [categories] : [1, 2, 3]});
    var search_string = document.getElementById('search-book').value;
    var class_num = parseInt(document.getElementById('classes-filter-select').value);
    var language = parseInt(document.getElementById('language-filter-select').value);
    var subject = parseInt(document.getElementById('subject-filter-select').value);
    var books_wrapper = document.getElementById('books-wrapper');
    var book_mockup = '<li onclick="showModal({0})" class="bookpage-book-item"> \
                           <a href="#" class="bookpage-book-item-link"><div style="width:99%"><div class="book-name-item">{1}</div><div class="book-author-item">{2}</div></div></a>\
                        </li>';
    var book_modal_mockup = '<div id="books-modal-{0}" open="false" class="modal"> \
                        <div class="modal-content"> \
                          <div class="modal-body"> \
                            <div class="book-item"> \
                              <div class="book-item-wrap"> \
                                <div class="book-item-img-wrap"> \
                                  <img id="book-logo" src="{4}" alt=""> \
                                </div> \
                                <div class="book-item-description"> \
                                  <h2 class="book-item-description-name">{1}</h2> \
                                  <p class="book-item-description-author">{2}</p> \
                                  <p class="book-description">{3}</p> \
                                </div> \
                              </div> \
                              <div class="book-button-wrap"> \
                                <a target="_blank" href="{5}">\
                                  <button class="btn-buy">Переглянути</button>\
                                </a>\
                                {6}\
                              </div> \
                            </div> \
                          </div> \
                        </div> \
                      </div>';
    var extra_data_btn_mockap = '<a target="_blank"><button class="btn-buy">Додатковi матерiали</button></a>';
    books_wrapper.innerHTML = "";

    if (class_num) {
        books_copy = books_copy.filter(satisfiesClass, {'class_num': class_num});
    }

    if (language) {
        books_copy = books_copy.filter(satisfiesLanguage, {'language': language});
    }
    if (subject) {
        books_copy = books_copy.filter(satisfiesSubject, {'subject': subject});
    }
    if (search_string) {
        books_copy = books_copy.filter(satisfiesSearch, {'search_string': search_string});
    }

    books_copy.forEach(function (book) {
        var extra_data_btn =  book['addition_url'] ? extra_data_btn_mockap: '';

        books_wrapper.insertAdjacentHTML('beforeend',
            book_mockup.format(book['id'], book['name'], book['author']));
        books_wrapper.insertAdjacentHTML('beforeend',
            book_modal_mockup.format(
                book['id'], book['name'], book['author'], book['description'], book['img_url'], book['book_url'], extra_data_btn
            ));
    });

}

// When the user clicks the button, open the modal
function showModal (book_id) {
    var modal = document.getElementById('books-modal-' + book_id);

    modal.setAttribute("open", "true");
    modal.style.display = "block";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    var modal = document.querySelector('.modal[open="true"]');
    if (event.target == modal) {
        modal.setAttribute("open", "false");
        modal.style.display = "none";
    }
};